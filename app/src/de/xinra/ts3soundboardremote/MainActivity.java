package de.xinra.ts3soundboardremote;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.net.*;
import java.util.ArrayList;

public class MainActivity extends Activity {
    private ArrayList<RemoteButton> remoteButtons;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        System.out.print("######## Das ist ein Test ########");

        LinearLayout layout = (LinearLayout) findViewById(R.id.buttonlayout);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        for(int i = 1; i <= 15; i++) {
            Button button = new Button(this);
            button.setText("Button " + i);
            final int id = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        InetAddress address = InetAddress.getByName("192.168.0.15");
                        byte[] message = ("/button " + id).getBytes();
                        DatagramPacket packet = new DatagramPacket(message, message.length, address, 19111);
                        DatagramSocket socket = new DatagramSocket();
                        socket.send(packet);
                        socket.close();
                    } catch (Exception e) {System.err.print(e.getMessage());}
                }
            });
            layout.addView(button, params);
        }
    }
}
