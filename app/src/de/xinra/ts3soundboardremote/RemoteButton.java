package de.xinra.ts3soundboardremote;

import java.io.Serializable;

/**
 * Erstellt von Erik Hofer am 30.04.2015.
 *
 * Represents a button in the app, can be serialized
 */
public class RemoteButton implements Serializable {
    private String label;
    private int trigger;

    public RemoteButton(String label, int trigger) {
        this.label = label;
        this.trigger = trigger;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getTrigger() {
        return trigger;
    }

    public void setTrigger(int trigger) {
        this.trigger = trigger;
    }
}
