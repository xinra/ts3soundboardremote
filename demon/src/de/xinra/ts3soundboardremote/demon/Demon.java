package de.xinra.ts3soundboardremote.demon;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Properties;

/**
 * Erstellt von Erik Hofer am 30.04.2015.
 *
 * The TS3 Soundboard Remote Demon connects the app to the Soundboard plugin
 */
public class Demon {
    public static void main(String[] args) {
        //default configuration
        Properties defaultProps = new Properties();
        defaultProps.setProperty("remote", "true");
        defaultProps.setProperty("listenPort", "19123");
        defaultProps.setProperty("soundboardPort", "19111");
        //defaultProps.setProperty("random", "false");

        final Properties props = new Properties(defaultProps);

        //if no configuration file exists, create one
        File propsFile = new File("demon.properties");
        if(!propsFile.exists() || propsFile.isDirectory()) {
            Writer writer = null;
            try {
                writer = new FileWriter(propsFile);
                defaultProps.store(writer, "TS3 Soundboard Remote Demon Configuration File (delete this file to restore the default one)");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    writer.close();
                } catch (IOException e) {}
            }
        } else { //Otherwise load configuration from file
            Reader reader = null;
            try {
                reader = new FileReader(propsFile);
                props.load(reader);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {}
            }
        }

        //create server thread for remote control
        if(Boolean.parseBoolean(props.getProperty("remote"))) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        DatagramSocket socket = new DatagramSocket(Integer.parseInt(props.getProperty("listenPort")));
                        byte[] receiveMessage = new byte[1024];
                        DatagramPacket receivePacket = new DatagramPacket(receiveMessage, receiveMessage.length);
                        while(true) { //server main loop
                            socket.receive(receivePacket);
                            byte[] sendMessage = receivePacket.getData(); //simply forward any message to the soundboard, CHANGE THIS IF YOU NEED MORE SECURITY
                            DatagramPacket sendPacket = new DatagramPacket(sendMessage, sendMessage.length, InetAddress.getByName("127.0.0.1"), Integer.parseInt(props.getProperty("soundboardPort")));
                            socket.send(sendPacket);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
        }
    }
}
